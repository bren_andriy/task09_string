package com.bren.view;

import com.bren.controller.SentenceController;
import com.bren.controller.SentenceControllerImpl;
import com.bren.model.SentenceService;

import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;
    private SentenceController sentenceController;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("11", bundle.getString("11"));
        menu.put("Q", bundle.getString("Q"));
    }

    public MyView() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        sentenceController = new SentenceControllerImpl();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::internationalizeMenuUkrainian);
        methodsMenu.put("3", this::internationalizeMenuEnglish);
        methodsMenu.put("4", this::internationalizeMenuBulgaria);
        methodsMenu.put("5", this::internationalizeMenuGermany);
        methodsMenu.put("6", this::internationalizeMenuFrench);
        methodsMenu.put("7", this::test2);
        methodsMenu.put("8", this::test3);
        methodsMenu.put("9", this::replaceAllVowels);
        methodsMenu.put("10", this::NumberOfSentenceWithDuplicates);
        methodsMenu.put("11", this::getTextSortedBySentenceSize);
    }

    private void testStringUtils() {
        System.out.println("First");
    }

    private void internationalizeMenuUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuBulgaria() {
        locale = new Locale("bg");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuGermany() {
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void internationalizeMenuFrench() {
        locale = new Locale("fra");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        show();
    }

    private void test2() {
        System.out.println("Sentence that begins with a capital letter and ends with a period.");
        String text = "Hello Andrii.";
        System.out.println(text);
        String regex = "^[A-Z].+\\.$";
        Pattern pattern = Pattern.compile(regex);
        Matcher m = pattern.matcher(text);
        while (m.find()) {
            System.out.print(text.substring(m.start(),m.end()) + "*");
        }
    }

    private void test3() {
        String text = "She has asked your brother to help them";
        System.out.println(text);
        System.out.println("Return value: ");
        for (String string : text.split("(the|you)+")) {
            System.out.print(string);
        }
        System.out.println();
    }

    private void replaceAllVowels() {
        String text = "EPAM is the best IT company in Ukraine";
        System.out.println(text);
        System.out.println("Return value: ");
        System.out.println(text.replaceAll("[aioeu]", "_"));
    }

    private void NumberOfSentenceWithDuplicates() throws IOException {
        System.out.println("Number of sentence with duplicates: " +
                sentenceController.getNumberOfSentenceWithDuplicates());
    }

    private void getTextSortedBySentenceSize() throws IOException {
        for (String[] sentence: sentenceController.getTextSortedBySentenceSize()) {
            for (String words : sentence) {
                System.out.print(words + " ");
            }
            System.out.println();
        }
    }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception ignored) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
