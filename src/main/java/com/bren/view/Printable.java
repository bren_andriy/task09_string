package com.bren.view;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
