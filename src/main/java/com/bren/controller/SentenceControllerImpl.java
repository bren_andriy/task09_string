package com.bren.controller;

import com.bren.model.SentenceBusinessLogic;
import com.bren.model.SentenceModel;

import java.io.IOException;
import java.util.List;

public class SentenceControllerImpl implements SentenceController{
    private SentenceModel sentenceModel;

    public SentenceControllerImpl() {
        sentenceModel = new SentenceBusinessLogic();
    }

    @Override
    public int getNumberOfSentenceWithDuplicates() throws IOException {
        return sentenceModel.getNumberOfSentenceWithDuplicates();
    }

    @Override
    public List<String[]> getTextSortedBySentenceSize() throws IOException {
        return sentenceModel.getTextSortedBySentenceSize();
    }
}
