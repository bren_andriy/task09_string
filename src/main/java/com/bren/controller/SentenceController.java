package com.bren.controller;

import java.io.IOException;
import java.util.List;

public interface SentenceController {
    int getNumberOfSentenceWithDuplicates() throws IOException;
    List<String[]> getTextSortedBySentenceSize() throws IOException;
}
