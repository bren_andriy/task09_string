package com.bren.model;


import java.io.IOException;
import java.util.List;

public interface SentenceModel {
    int getNumberOfSentenceWithDuplicates() throws IOException;
    List<String[]> getTextSortedBySentenceSize() throws IOException;
}
