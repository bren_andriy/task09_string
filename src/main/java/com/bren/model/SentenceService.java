package com.bren.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceService {
    private static String words;
    private static Pattern pattern;
    private static Matcher matcher;

    int getNumberOfSentenceWithDuplicates() throws IOException {
        words = getWords("/Users/admin/Epam_Java/9_Ninth_Task/task1.txt");
        String regex = "(\\w|^\\s)+([^.!?]*)"; //find each sentence
        String string = words.replaceAll("(\t|\\s)+", " ");
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(string);
        int count = 0;
        while (matcher.find()) {
            String str = matcher.group().replaceAll("\\W", " ");
            String[] separatedWords = str.split(",");
            if (isDuplicates(separatedWords)) {
                count++;
            }
        }
        return count;
    }

    List<String[]> getTextSortedBySentenceSize() throws IOException {
        words = getWords("/Users/admin/Epam_Java/9_Ninth_Task/task2.txt");
        String regex = "(\\w|^\\s)+([^.!?]*)";
        String string = words.replaceAll("(\t|\\s)+", " ");
        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(string);
        List<String[]> text = new ArrayList<>();
        while (matcher.find()) {
            String[] sentences = matcher.group().split(" ");
            text.add(sentences);
        }
        text.sort(Comparator.comparing(strings -> strings.length));
        return text;
    }

    private static boolean isDuplicates(String[] words) {
        for (int i = 0; i < words.length - 1; i++) {
            if (words[i].equalsIgnoreCase(words[i + 1])) {
                return true;
            }
        }
        return false;
    }

    private static String getWords(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)));
    }


}

