package com.bren.model;

import java.io.IOException;
import java.util.List;

public class SentenceBusinessLogic implements SentenceModel {
    private SentenceService sentenceService;

    public SentenceBusinessLogic() {
        sentenceService = new SentenceService();
    }

    @Override
    public int getNumberOfSentenceWithDuplicates() throws IOException {
        return sentenceService.getNumberOfSentenceWithDuplicates();
    }

    @Override
    public List<String[]> getTextSortedBySentenceSize() throws IOException {
        return sentenceService.getTextSortedBySentenceSize();
    }
}
